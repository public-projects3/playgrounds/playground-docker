# Overview

This is a playground primarily for Docker. It also includes the following:

Docker
Go
Java
Node.js
C/C++
Python
Ruby
Rust
Clojure
Homebrew
Tailscale
Nginx
and several more